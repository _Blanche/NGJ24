using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float magnitude;
    public float frequency = 5;
    public float decay = 0.1f;

    private Vector3 startPos = new Vector3 (0, 0, 0);
    private float curMagnitude;
    public Vector3 direction = Vector3.zero;

    private void OnEnable()
    {
        GlobalEvents.onEvent += OnGlobalEvent;
    }

    private void OnDisable()
    {
        GlobalEvents.onEvent -= OnGlobalEvent;
    }

    void OnGlobalEvent(GEvent gEvent)
    {
        if (gEvent.type == GEventType.SHIELD_REFLECT)
        {
            AddScreenshake(gEvent.vector, 0.15f);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    public void AddScreenshake(Vector3 dir, float magnitude)
    {
        curMagnitude = magnitude;
        direction = dir.normalized;
    }

    // Update is called once per frame
    void Update()
    {
        // debug
        /*if (Input.GetKeyDown("space"))
        {
            curMagnitude = magnitude;
        }*/

        if (curMagnitude >= 0)
        {
            curMagnitude = curMagnitude - Time.deltaTime * decay;
        }

        transform.position = startPos + direction * Mathf.Sin(Time.time * frequency) * curMagnitude;

        
    }
}
