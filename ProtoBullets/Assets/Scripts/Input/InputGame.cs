using UnityEngine;

public class InputGame : InputState
{
    public float MovementGamepadThreshold = .1f;

    float _LeftAxisX;
    float _LeftAxisY;
    float _RightAxisX;
    float _RightAxisY;
    bool _AButton;

    bool _ShouldMove;
    bool _ShouldLook;

    IMoveable _MoveableThingy;
    InputThingy _Controller;

    public 
    InputGame(InputThingy inputThingy)
    {
        _Controller = inputThingy;
    }

    public void
    AttachMoveable(IMoveable moveablethingy)
    {
        _MoveableThingy = moveablethingy;
    }

    internal void
    ResetMoveable()
    {
        _MoveableThingy = null;
    }

    public override void 
    Update(float deltaTime)
    {
        _ShouldMove = false;
        _ShouldLook = false;

        if (Mathf.Abs(Input.GetAxis(_Controller.Player_Vertical_Left_Axis)) > MovementGamepadThreshold)
        {
            _ShouldMove = true;
            _LeftAxisY = Input.GetAxis(_Controller.Player_Vertical_Left_Axis);
        }
        if (Mathf.Abs(Input.GetAxis(_Controller.Player_Horizontal_Left_Axis)) > MovementGamepadThreshold)
        {
            _ShouldMove = true;
            _LeftAxisX = Input.GetAxis(_Controller.Player_Horizontal_Left_Axis);
        }

        if (_Controller.DeviceUsed == Devices.KeyboardNMouse)
        {
            _ShouldLook = true;
            _RightAxisX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
            _RightAxisY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
        }
        else
        {
            if (Mathf.Abs(Input.GetAxis(_Controller.Player_Vertical_Right_Axis)) > MovementGamepadThreshold)
            {
                _ShouldLook = true;
                _RightAxisY = Input.GetAxis(_Controller.Player_Vertical_Right_Axis);
            }
            if (Mathf.Abs(Input.GetAxis(_Controller.Player_Horizontal_Right_Axis)) > MovementGamepadThreshold)
            {
                _ShouldLook = true;
                _RightAxisX = Input.GetAxis(_Controller.Player_Horizontal_Right_Axis);
            }
        }
        _AButton = Input.GetButtonDown(_Controller.Player_A_Button);

        if (_AButton)
        {
            _MoveableThingy.OnAButton();
        }
    }

    public override void
    FixedUpdate(float fixedDeltaTime)
    {
        if (_ShouldMove)
        {
            Vector2 Movement = (Vector2.right * _LeftAxisX) + (Vector2.up * _LeftAxisY);
            _MoveableThingy.Move(Movement.normalized, Movement.magnitude);
        }
        else
        {
            _MoveableThingy.Move(Vector3.zero, 0f);
        }

        if (_ShouldLook)
        {
            _MoveableThingy.Look((Vector2.right * _RightAxisX) + (Vector2.up * _RightAxisY), _Controller.DeviceUsed);
        }
    }
}
