using UnityEngine;

public class InputMenu : InputState
{
    public float MovementGamepadThreshold = .1f;
    
    float _LeftAxisX;
    bool _AButton;
    bool _BButton;

    bool _SelectionEnabled;

    IUIGamepadable _Gamepadable;
    InputThingy _Controller;

    public
    InputMenu(InputThingy inputThingy)
    {
        _Controller = inputThingy;
    }

    public void
    AttachGamepadable(IUIGamepadable gamepadablethingy)
    {
        _Gamepadable = gamepadablethingy;
    }

    internal void
    ResetGamepadable()
    {
        _Gamepadable = null;
    }

    public override void
    Update(float deltaTime)
    {
        _AButton = Input.GetButtonDown(_Controller.Player_A_Button);
        if (_AButton)
        {
            //if (!_SelectionEnabled)
            //{
                _SelectionEnabled = true;
                _Gamepadable.EnableSelection();
            //}
        }

        _BButton = Input.GetButtonDown(_Controller.Player_B_Button);
        if (_BButton)
        {
            if (_SelectionEnabled)
            {
                _SelectionEnabled = false;
                _Gamepadable.DisableSelection();
            }
        }

        if (_SelectionEnabled)
        {
            if (Mathf.Abs(Input.GetAxis(_Controller.Player_Horizontal_Left_Axis)) > MovementGamepadThreshold)
            {
                _LeftAxisX = Input.GetAxis(_Controller.Player_Horizontal_Left_Axis);
                if (Mathf.Sign(_LeftAxisX) > 0f)
                {
                    _Gamepadable.GoRight();
                }
                else if (Mathf.Sign(_LeftAxisX) < 0f)
                {
                    _Gamepadable.GoLeft();
                }
            }
        }
    }

    public override void 
    FixedUpdate(float fixedDeltaTime)
    { }
}
