using UnityEngine;

public class InputBeginGame : InputState
{
    public float MovementMouseThreshold = 1f;
    public float MovementGamepadThreshold = .1f;

    float _RightAxisX;
    float _RightAxisY;
    bool _AButton;

    bool _ShouldLook;

    IMoveable _MoveableThingy;
    InputThingy _Controller;

    public
    InputBeginGame(InputThingy inputThingy)
    {
        _Controller = inputThingy;
    }

    public void
    AttachMoveable(IMoveable moveablethingy)
    {
        _MoveableThingy = moveablethingy;
    }

    internal void
    ResetMoveable()
    {
        _MoveableThingy = null;
    }

    public override void
    Update(float deltaTime)
    {
        _ShouldLook = false;

        if (_Controller.DeviceUsed == Devices.KeyboardNMouse)
        {
            _ShouldLook = true;
            _RightAxisX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
            _RightAxisY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
        }
        else
        {
            if (Mathf.Abs(Input.GetAxis(_Controller.Player_Vertical_Right_Axis)) > MovementGamepadThreshold)
            {
                _ShouldLook = true;
                _RightAxisY = Input.GetAxis(_Controller.Player_Vertical_Right_Axis);
            }
            if (Mathf.Abs(Input.GetAxis(_Controller.Player_Horizontal_Right_Axis)) > MovementGamepadThreshold)
            {
                _ShouldLook = true;
                _RightAxisX = Input.GetAxis(_Controller.Player_Horizontal_Right_Axis);
            }
        }
        _AButton = Input.GetButtonDown(_Controller.Player_A_Button);
    }

    public override void
    FixedUpdate(float fixedDeltaTime)
    {
        if (_ShouldLook)
        {
            _MoveableThingy.Look((Vector2.right * _RightAxisX) + (Vector2.up * _RightAxisY), _Controller.DeviceUsed);
        }

        if (_AButton)
        {
            _MoveableThingy.OnAButton();
        }
    }
}
