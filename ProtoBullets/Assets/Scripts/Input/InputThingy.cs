using UnityEngine;

public class InputThingy
{
    const string _VERTICAL_LEFT_AXIS = "VerticalLeft";
    const string _HORIZONTAL_LEFT_AXIS = "HorizontalLeft";
    const string _VERTICAL_RIGHT_AXIS = "VerticalRight";
    const string _HORIZONTAL_RIGHT_AXIS = "HorizontalRight";
    const string _A_BUTTON = "A";
    const string _B_BUTTON = "B";

    internal string Player_Vertical_Left_Axis;
    internal string Player_Horizontal_Left_Axis;
    internal string Player_Vertical_Right_Axis;
    internal string Player_Horizontal_Right_Axis;
    internal string Player_A_Button;
    internal string Player_B_Button;

    public float MovementMouseThreshold = 1f;

    internal Devices DeviceUsed = Devices.KeyboardNMouse;

    public int PlayerIndex { get; private set; }

    Vector3 _PreviousMousePosition;

    InputDisabled _CachedStateDisabled;
    InputMenu _CachedStateMenu;
    InputGame _CachedStateGame;
    InputBeginGame _CachedStateBeginGame;
    InputState _CurrentState;

    public
    InputThingy(int playerindex)
    {
        PlayerIndex = playerindex;

        Player_Vertical_Left_Axis = _VERTICAL_LEFT_AXIS + PlayerIndex.ToString();
        Player_Horizontal_Left_Axis = _HORIZONTAL_LEFT_AXIS + PlayerIndex.ToString();
        Player_Vertical_Right_Axis = _VERTICAL_RIGHT_AXIS + PlayerIndex.ToString();
        Player_Horizontal_Right_Axis = _HORIZONTAL_RIGHT_AXIS + PlayerIndex.ToString();
        Player_A_Button = _A_BUTTON + PlayerIndex.ToString();
        Player_B_Button = _B_BUTTON + PlayerIndex.ToString();

        _CachedStateDisabled = new InputDisabled();
        _CachedStateMenu = new InputMenu(this);
        _CachedStateGame = new InputGame(this);
        _CachedStateBeginGame = new InputBeginGame(this);
        _CurrentState = _CachedStateDisabled;
    }

    internal void 
    SwitchToGameState(IMoveable moveablethingy)
    {
        _CachedStateGame.AttachMoveable(moveablethingy);
        _CurrentState = _CachedStateGame;
        _CachedStateBeginGame.ResetMoveable();
        _CachedStateMenu.ResetGamepadable();
    }

    internal void
    SwitchToBeginGameState(IMoveable moveablethingy)
    {
        _CachedStateBeginGame.AttachMoveable(moveablethingy);
        _CurrentState = _CachedStateBeginGame;
        _CachedStateGame.ResetMoveable();
        _CachedStateMenu.ResetGamepadable();
    }

    internal void
    SwitchToMenuState(IUIGamepadable gamepadablethingy)
    {
        _CachedStateMenu.AttachGamepadable(gamepadablethingy);
        _CurrentState = _CachedStateMenu;
        _CachedStateBeginGame.ResetMoveable();
        _CachedStateGame.ResetMoveable();
    }

    internal void
    SwitchToDisabledState()
    {
        _CurrentState = _CachedStateDisabled;
        _CachedStateBeginGame.ResetMoveable();
        _CachedStateGame.ResetMoveable();
        _CachedStateMenu.ResetGamepadable();
    }

    internal void 
    Update(float deltaTime)
    {
        DeviceUsed = Mathf.Abs((Input.mousePosition - _PreviousMousePosition).magnitude) > MovementMouseThreshold ? Devices.KeyboardNMouse : Devices.Gamepad;

        _CurrentState.Update(deltaTime);

        _PreviousMousePosition = Input.mousePosition;
    }

    internal void 
    FixedUpdate(float fixedDeltaTime)
    {
        _CurrentState.FixedUpdate(fixedDeltaTime);
    }
}
