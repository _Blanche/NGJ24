public abstract class InputState
{
    public abstract void
    Update(float deltaTime);

    public abstract void
    FixedUpdate(float fixedDeltaTime);
}
