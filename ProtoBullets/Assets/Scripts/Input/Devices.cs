public enum Devices
{
    None,
    Keyboard,
    KeyboardNMouse,
    Mouse,
    Gamepad
}