using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    //public AudioClip explosionSound;
    //public Vector2 explosionPitchRange;
    //public Vector2 explosionVolumeRange;
    public static AudioPlayer instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = Instantiate(new GameObject("Audio Player"));
                _instance = go.AddComponent<AudioPlayer>();
            }
            return _instance;
        }
    }

    private static AudioPlayer _instance;

    // Start is called before the first frame update
    void OnEnable()
    {
        //GlobalEvents.instance.onEvent += ReactToEvent;
    }

    private void OnDisable()
    {
        //GlobalEvents.instance.onEvent -= ReactToEvent;
    }

    /*void ReactToEvent(GEvent gEvent)
    {
        if (gEvent.name == "Explosion")
        {
            PlayAudio(explosionSound, Random.Range(explosionVolumeRange.x, explosionVolumeRange.y), Random.Range(explosionPitchRange.x, explosionPitchRange.y));
        }
    }*/

    public void PlayAudio(AudioClip audioClip, float volume = 1, float pitch = 1)
    {
        AudioSource nSource = gameObject.AddComponent<AudioSource>();
        nSource.spatialBlend = 0;
        nSource.clip = audioClip;
        nSource.volume = volume;
        nSource.pitch = pitch;
        nSource.Play();
        Destroy(nSource, audioClip.length / pitch);
    }
}
