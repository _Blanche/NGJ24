using UnityEngine.SceneManagement;

public class SceneHandler
{
    internal event System.Action SceneLoaded;

    internal void 
    Load(string scenename, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.LoadSceneAsync(scenename, mode);
    }

    void 
    OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        SceneLoaded?.Invoke();
    }
}