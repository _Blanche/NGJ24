using UnityEngine;

public interface IMoveable
{
    event System.Action Died;

    void Move(Vector2 direction, float magnitude);
    void Look(Vector2 target, Devices deviceused);

    public void OnAButton();
}