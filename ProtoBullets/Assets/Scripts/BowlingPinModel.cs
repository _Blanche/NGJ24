using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlingPinModel : MonoBehaviour
{
    public GameObject player;
    public GameObject pin;
    public GameObject helmet;
    public GameObject shield;

    public float rotationOffset = 90;

    public bool moving = false;

    public Vector3 positionOffset = new Vector3();

    public Color playerColor = Color.red;

    public float jumpingSpeed = 20f;
    public float jumpingHeight = 0.5f;

    public float animationMomentum = 0f, animationMomentumBuildup = 5f, animationMomentumFalloff = 2.5f;

    public Feather feather;
    public Renderer stripe1, stripe2;

    public ParticleSystem shieldDeflectParticle, shoveParticleSystem;
    public AudioClip shieldGuardHitSound, pinHit, shieldBackSFX, shieldPushSFX, dieSFX;

    private void OnEnable()
    {
        SetColor(playerColor);
        lastPos = transform.position;
        GlobalEvents.onEvent += OnGlobalEvent;
    }

    private void OnDisable()
    {
        GlobalEvents.onEvent -= OnGlobalEvent;
    }

    public void Initialise(Character character)
    {
        player = character.gameObject;
    }

    void OnGlobalEvent(GEvent gEvent)
    {
        if (gEvent.type == GEventType.SHIELD_REFLECT)
        {
            if (gEvent.sender == player.gameObject)
            {
                shieldDeflectParticle.Emit(10);
                AudioPlayer.instance.PlayAudio(shieldGuardHitSound, 1f, Random.Range(1f, 1.3f));
            }
        }
        else if (gEvent.type == GEventType.HIT)
        {
            if (gEvent.sender == player.gameObject)
            {
                AudioPlayer.instance.PlayAudio(pinHit, 1f, Random.Range(0.9f, 1f));
                StartCoroutine(HitRoutine());
            }
        }
        else if (gEvent.type == GEventType.DIE)
        {
            if (gEvent.sender == player.gameObject)
            {
                AudioPlayer.instance.PlayAudio(dieSFX, 1f, Random.Range(0.9f, 1f));
                StartCoroutine(DieRoutine());
            }
        }
        else if (gEvent.type == GEventType.SHIELD_SHOVE)
        {
            if (gEvent.sender == player.gameObject)
            {
                switch (gEvent.iValue)
                {
                    case 0:
                        LeanTween.moveLocalX(shield, 0.2f, 0.3f).setEaseOutBack();
                        AudioPlayer.instance.PlayAudio(shieldBackSFX, 1f, 1.5f);
                        break;
                    case 1:
                        LeanTween.moveLocalX(shield, 1.4f, 0.2f).setEaseOutBounce().setEaseOutBounce();
                        shoveParticleSystem.Emit(20);
                        AudioPlayer.instance.PlayAudio(shieldPushSFX, 1f, 1f);
                        break;
                    case 2: LeanTween.moveLocalX(shield, 0.81f, 0.1f).setEaseOutCirc(); break;
                }
            }
        }
    }

    IEnumerator HitRoutine()
    {
        Time.timeScale = 0.1f;
        takingDamage = true;
        StartCoroutine(TakeDamageAnimRoutine());
        yield return new WaitForSecondsRealtime(0.05f);
        float t = 0;
        while (Time.timeScale < 1f)
        {
            Time.timeScale = Mathf.MoveTowards(Time.timeScale, 1f, Time.unscaledDeltaTime * 5);
            yield return null;
        }
    }

    IEnumerator DieRoutine()
    {
        Time.timeScale = 0.01f;

        yield return new WaitForSecondsRealtime(0.2f);
        float t = 0;
        while (Time.timeScale < 1f)
        {
            Time.timeScale = Mathf.MoveTowards(Time.timeScale, 1f, Time.unscaledDeltaTime * 0.5f);
            yield return null;
        }
    }

    bool takingDamage = false;

    IEnumerator TakeDamageAnimRoutine()
    {
        pin.transform.localPosition = Vector3.up;

        LeanTween.moveLocalY(pin, 0, 0.4f).setEaseInExpo().setEaseOutBounce();
        Vector3 fromRot = pin.transform.localEulerAngles;
        pin.transform.localEulerAngles = new Vector3(Random.Range(-20f, 20f), fromRot.y, Random.Range(-20f, 20f));
        LeanTween.rotateLocal(pin, fromRot, 0.4f).setEaseInOutBack();
        yield return new WaitForSeconds(0.4f);
        takingDamage = false;
    }

    public void SetColor(Color color)
    {
        feather.line.material.color = color;
        stripe1.material.color = color;
        stripe2.material.color = color;
    }

    Vector3 lastPos;



    private void Update()
    {
        
        if (moving)
        {
            animationMomentum += Time.deltaTime * animationMomentumBuildup;
        }
        else
        {
            animationMomentum -= Time.deltaTime * animationMomentumFalloff;
        }

        animationMomentum = Mathf.Clamp(animationMomentum, 0f, 1f);

        if (!takingDamage)
            pin.transform.localPosition = new Vector3(0, Mathf.Abs(Mathf.Sin(Time.time * jumpingSpeed))*jumpingHeight *animationMomentum, 0);
    }

    private void FixedUpdate()
    {
        // Debug.Log((transform.position - lastPos).magnitude);
        moving = ((transform.position - lastPos).magnitude > Time.deltaTime);

        lastPos = transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player == null)
            return;
        transform.position = player.transform.position + positionOffset;
        Vector3 playerRot = player.transform.eulerAngles;

        if (!takingDamage)
            pin.transform.localEulerAngles = new Vector3(0, -playerRot.z + rotationOffset, 0);
    }
}
