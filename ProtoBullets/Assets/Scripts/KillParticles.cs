using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillParticles : MonoBehaviour
{
    public static KillParticles instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = Instantiate(Resources.Load<GameObject>("Kill Particles"));
                _instance = go.GetComponent<KillParticles>();
            }
            return _instance;
        }
    }

    private static KillParticles _instance;

    private void OnDestroy()
    {
        if (_instance == this)
            _instance = null;
    }

    public ParticleSystem blood, pins;

    public void ShowKill(Vector3 pos)
    {
        pins.transform.position = pos;
        blood.transform.position = pos;
        pins.Emit(10);
        blood.Emit(20);
    }
}
