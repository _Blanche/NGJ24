public class GameRun : GameState
{
    public event System.Action<bool> GameRunEnded;

    public int PlayersAlive;

    const float _END_CHECK_DURATION = 1f;
    float _ElapsedTime;
    bool _ClockRunning;

    public override void 
    Start()
    {
        _ElapsedTime = 0f;
        for (int i = 0; i < Director.CurrentUsersCount; i++)
        {
            Director.Players[i].StartPlaying();
        }
    }

    public override void 
    Update(float deltatime)
    {
        for (int i = 0; i < Director.CurrentUsersCount; i++)
        {
            Director.Players[i].Update(deltatime);
        }

        if (_ClockRunning)
        {
            _ElapsedTime += deltatime;
            if (_ElapsedTime > _END_CHECK_DURATION)
            {
                _ClockRunning = false;
                _ElapsedTime = 0f;

                if (PlayersAlive <= 1)
                {
                    End();
                }
            }
        }
    }

    public void
    OnPlayerDied()
    {
        PlayersAlive--;
        _ClockRunning = true;
        _ElapsedTime = 0f;
    }

    public override void 
    End()
    {
        bool Draw = PlayersAlive == 0;
        GameRunEnded?.Invoke(Draw);
    }
}
