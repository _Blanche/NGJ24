public abstract class GameState
{
    public abstract void
    Start();
    public abstract void
    Update(float deltatime);
    public abstract void
    End();
}