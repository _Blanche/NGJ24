using UnityEngine;

public class GameBegin : GameState
{
    public event System.Action GameBeginEnded;

    const float _BEGINNING_DURATION = 3f;
    float _ElapsedTime;
    bool _ClockRunning;

    public override void 
    Start()
    {
        switch (Director.CurrentUsersCount)
        {
            case 2:
                for (int i = 0; i < Director.CurrentUsersCount; i++)
                {
                    Director.Players[i].AttachController(Director.Builder.SpawnPlayerPawn(Director.CurrentLevel.SpawnPoints2Player, i));
                }
                break;
            case 3:
                for (int i = 0; i < Director.CurrentUsersCount; i++)
                {
                    Director.Players[i].AttachController(Director.Builder.SpawnPlayerPawn(Director.CurrentLevel.SpawnPoints3Player, i));
                }
                break;
            case 4:
                for (int i = 0; i < Director.CurrentUsersCount; i++)
                {
                    Director.Players[i].AttachController(Director.Builder.SpawnPlayerPawn(Director.CurrentLevel.SpawnPoints4Player, i));
                }
                break;
            default: Debug.Log("Argh");
                break;
        }

        for (int j = 0; j < Director.CurrentUsersCount; j++)
        {
            Director.Players[j].WaitToPlay();
        }

        _ElapsedTime = 0f;
        _ClockRunning = true;
    }

    public override void 
    Update(float deltatime)
    {
        for (int i = 0; i < Director.CurrentUsersCount; i++)
        {
            Director.Players[i].Update(deltatime);
        }

        if (_ClockRunning)
        {
            _ElapsedTime += deltatime;
            if (_ElapsedTime > _BEGINNING_DURATION)
            {
                _ClockRunning = false;
                End();
            }
        }
    }

    public override void 
    End()
    {
        GameBeginEnded?.Invoke();
    }
}
