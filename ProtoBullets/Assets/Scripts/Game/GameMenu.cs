public class GameMenu : GameState
{
    public override void
    Start()
    { }

    public override void 
    Update(float deltatime)
    {
        for (int i = 0; i < Director.Players.Length; i++)
        {
            Director.Players[i].Update(deltatime);
        }
    }

    public override void 
    End()
    {
    }
}
