using UnityEngine;

public class DirectorView : MonoBehaviour
{
    public Director Director;

    Level _Level;

    void 
    Awake()
    {
        _Level = GetComponent<Level>();
        Director.Game_SetCurrentLevel(_Level);
    }

    void 
    Update()
    {
        Director.MyUpdate(Time.deltaTime);
    }

    void
    FixedUpdate()
    {
        Director.MyFixedUpdate(Time.fixedDeltaTime);
    }
}
