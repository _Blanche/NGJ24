public class GameEnd : GameState
{ 
    public event System.Action GameEndEnded;

    public bool Draw;

    public override void 
    Start()
    {
    }

    public override void 
    Update(float deltatime)
    {
        for (int i = 0; i < Director.CurrentUsersCount; i++)
        {
            Director.Players[i].Update(deltatime);
        }
    }

    public override void 
    End()
    {
        GameEndEnded?.Invoke();
    }
}
