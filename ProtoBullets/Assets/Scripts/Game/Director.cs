using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "Director", menuName = "ScriptableObjects/Director")]
public class Director : ScriptableObject
{
    public event System.EventHandler RoundStarted;
    public event System.EventHandler RoundEnded;

    static Director _Instance;

    int _CurrentUsersCount;
    public static int CurrentUsersCount { get { return _Instance._CurrentUsersCount; } }
    Player[] _Players;
    public static Player[] Players { get { return _Instance._Players; } }

    SceneHandler _SceneHandler;

    GameBegin _CachedGameBegin;
    GameRun _CachedGameRun;
    GameEnd _CachedGameEnd;
    GameMenu _CachedGameMenu;
    GameState _CurrentGameState;

    public Builder _Builder;
    public static Builder Builder { get { return _Instance._Builder; } }

    Level _CurrentLevel;
    public static Level CurrentLevel { get { return _Instance._CurrentLevel; } }

    public LevelNames MenuScene;
    public LevelNames[] Levels;

    internal void 
    Initialise()
    {
        _Instance = this;

        _CachedGameBegin = new GameBegin();
        _CachedGameRun = new GameRun();
        _CachedGameEnd = new GameEnd();
        _CachedGameMenu = new GameMenu();
        _CurrentGameState = _CachedGameMenu;

        _Players = new Player[4];
        for (int i = 0; i < _Players.Length; i++)
        {
            _Players[i] = new Player();
            _Players[i].Initialise(i);
            _Players[i].Died += Game_OnPlayerDied;
        }

        _SceneHandler = new SceneHandler();
        _SceneHandler.Load(MenuScene.ToString(), LoadSceneMode.Single);
    }

    internal void 
    MyUpdate(float deltaTime)
    {
        _CurrentGameState.Update(deltaTime);
    }

    internal void 
    MyFixedUpdate(float fixedDeltaTime)
    {
        for (int i = 0; i < _CurrentUsersCount; i++)
        {
            _Players[i].FixedUpdate(fixedDeltaTime);
        }
    }

    internal static void
    Menu_LoadingScene()
    {
        for (int i = 0; i < _Instance._Players.Length; i++)
        {
            _Instance._Players[i].ControlMenu();
        }
    }

    internal static void
    Menu_SetCurrentUsersCount(int count)
    {
        _Instance._CurrentUsersCount = count;
    }

    internal void
    Game_SetCurrentLevel(Level level)
    {
        _CurrentLevel = level;
    }

    void
    Game_OnLevelLoaded()
    {
        _SceneHandler.SceneLoaded -= Game_OnLevelLoaded;

        _CachedGameBegin.GameBeginEnded += Game_OnBeginEnded;
        _CurrentGameState = _CachedGameBegin;
        _CurrentGameState.Start();
    }

    internal static void
    Game_Start()
    {
        _Instance._SceneHandler.SceneLoaded += _Instance.Game_OnLevelLoaded;
        _Instance._SceneHandler.Load(_Instance.Levels[0].ToString(), LoadSceneMode.Single);
    }

    void 
    Game_OnBeginEnded()
    {
        _CachedGameBegin.GameBeginEnded -= Game_OnBeginEnded;
    
        _CachedGameRun.GameRunEnded += Game_OnRunEnded;
        _CachedGameRun.PlayersAlive = _CurrentUsersCount;
        _CurrentGameState = _CachedGameRun;
        _CurrentGameState.Start();
    }

    void
    Game_OnPlayerDied()
    {
        _CachedGameRun.OnPlayerDied();
    }

    void 
    Game_OnRunEnded(bool draw)
    {
        _CachedGameRun.GameRunEnded -= Game_OnRunEnded;

        _CachedGameEnd.GameEndEnded += Game_OnEndEnded;
        _CachedGameEnd.Draw = draw;
        if (!draw)
        {
            for (int i = 0; i < _CurrentUsersCount; i++)
            {
                if (!_Players[i].IsDead)
                {
                    //this is the winner
                    break;
                }
            }
        }
        _CurrentGameState = _CachedGameEnd;
        _CurrentGameState.Start();
    }

    private void Game_OnEndEnded()
    {
    }
}
