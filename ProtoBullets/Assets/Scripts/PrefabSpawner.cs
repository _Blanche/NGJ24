using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabSpawner : MonoBehaviour
{
    [SerializeField] private float _initialSpawnDelay;
    [SerializeField] private float _spawnInterval;
    [SerializeField] private GameObject _prefabToSpawn;
    [SerializeField] private Transform _spawnLocation;
    private float _timeSinceSpawn;
    

    private void OnEnable()
    {
        _timeSinceSpawn = _spawnInterval;
    }

    void Update()
    {
        if (_initialSpawnDelay > 0)
        {
            _initialSpawnDelay -= Time.deltaTime;
            return;
        }
        
        _timeSinceSpawn += Time.deltaTime;

        if (_timeSinceSpawn>_spawnInterval)
        {
            Instantiate(_prefabToSpawn, _spawnLocation);
            _timeSinceSpawn = 0;
        }
    }
}
