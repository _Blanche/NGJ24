using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private float _baseSpeed;

    [SerializeField] private float colliderSize;

    [SerializeField] private float skinWidth = 0.01f;
    
    

    [SerializeField] private LayerMask _bulletCollisionLayers;

    // Visuals
    [SerializeField] private Transform ballModel;
    [SerializeField] private float ballVisualRotationSpeed = 1f;


    private float currentSpeed;
    public Vector2 currentDirection;
    private int damage;

    private Vector2 storedOffsetFromShieldCenter;
    private Rigidbody2D storedRigidbody;
    private bool ismagnetizedtoShield;
    public AudioClip wallBounceAudio;

    public void Initialize(Vector3 initDirection)
    {
        currentSpeed = _baseSpeed;
        currentDirection = initDirection;
        GetComponent<AudioSource>().time = UnityEngine.Random.Range(0, 3f);
    }

    public float shieldSpeedBoost = 4f, shieldBoostDropoffSpeed = 4f;
    public ParticleSystem boostParticles;

    private void FixedUpdate()
    {
        // if (ismagnetizedtoShield)
        // {
        //     _rigidbody.MovePosition(_rigidbody.position + storedOffsetFromShieldCenter);
        //     return;
        // }
        currentSpeed = Mathf.MoveTowards(currentSpeed, _baseSpeed, Time.fixedDeltaTime * shieldBoostDropoffSpeed);
        
        var positionDelta =  currentDirection * currentSpeed * Time.fixedDeltaTime;
        
        var oldPosition = _rigidbody.position;
        var newPosition = _rigidbody.position + positionDelta;



        var rayHit = Physics2D.Raycast(oldPosition, positionDelta.normalized, positionDelta.magnitude + colliderSize,
            _bulletCollisionLayers);
        //var rayHit = Physics2D.CircleCast(oldPosition, colliderSize, positionDelta.normalized, positionDelta.magnitude,
        //    _bulletCollisionLayers);

        if (rayHit.collider != null)
        {
            newPosition = rayHit.point - positionDelta.normalized * (colliderSize + skinWidth);
            if (rayHit.collider.CompareTag("Wall"))
            {
                var ge = new GEvent();
                ge.type = GEventType.WALL_BOUNCE;
                ge.sender = gameObject;
                GlobalEvents.TriggerEvent(ge);
                AudioPlayer.instance.PlayAudio(wallBounceAudio, UnityEngine.Random.Range(0.2f, 0.4f), UnityEngine.Random.Range(0.4f, 0.6f));
            }
            else if (rayHit.collider.CompareTag("Shield"))
            {
                ismagnetizedtoShield = true;
                storedRigidbody = rayHit.collider.attachedRigidbody;
                storedOffsetFromShieldCenter = rayHit.point - storedRigidbody.position;

                float targetBoostSpeed = _baseSpeed + shieldSpeedBoost;
                if (targetBoostSpeed > currentSpeed)
                    currentSpeed = targetBoostSpeed;

                var newDir = Vector2.Reflect(currentDirection, rayHit.normal);

                var shieldEvent = new GEvent();
                shieldEvent.type = GEventType.SHIELD_REFLECT;
                shieldEvent.sender = rayHit.collider.transform.root.gameObject;
                shieldEvent.vector = newDir.normalized;
                GlobalEvents.TriggerEvent(shieldEvent);
            }
            else if (rayHit.collider.CompareTag("Player"))
            {
                var characterScript = rayHit.collider.GetComponent<Character>();
                characterScript.DealDamage(1);
            }
            else if (rayHit.collider.CompareTag("Barrel"))
            {
                var barrelController = rayHit.collider.GetComponent<DestructibleBarrel>();
                barrelController.DealDamage();
            }
            
            var newDirection = Vector2.Reflect(currentDirection, rayHit.normal);
            currentDirection = newDirection;
        }
        
        _rigidbody.MovePosition(newPosition); 
    }

    public void ShieldBashed()
    {
        currentSpeed = _baseSpeed + (shieldSpeedBoost * 3);
    }

    private void LateUpdate()
    {   
        var emission = boostParticles.emission;
        emission.rateOverDistance = (currentSpeed - _baseSpeed) * 5f;
        

        // Update ball GRAPHICS rotation crap :D
        ballModel.transform.Rotate(Vector3.forward, ballVisualRotationSpeed * currentDirection.y);
        ballModel.transform.Rotate(Vector3.left, ballVisualRotationSpeed * currentDirection.x);
    }
}