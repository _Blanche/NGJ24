using UnityEngine;

public class RotatorScript : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed = 1;
    
    void FixedUpdate()
    {
        transform.Rotate(Vector3.forward, Time.deltaTime*_rotationSpeed);
    }
}
