public class Player
{
    public event System.Action Died;

    InputThingy _Input;

    IMoveable _MoveableThingy;
    IUIGamepadable _GamepadableThingy;

    int _Index;

    public bool IsDead { get; private set; }

    public Player()
    {

    }

    internal void
    Initialise(int index)
    {
        _Index = index;
        _Input = new InputThingy(_Index);
    }

    internal void
    Update(float deltaTime)
    {
        _Input.Update(deltaTime);
    }

    internal void
    FixedUpdate(float fixedDeltaTime)
    {
        _Input.FixedUpdate(fixedDeltaTime);
    }

    internal void
    AttachController(IMoveable moveablethingy)
    {
        _MoveableThingy = moveablethingy;
        _MoveableThingy.Died += OnThingyDied;
    }

    internal void
    AttachUISelection(IUIGamepadable gamepadablethingy)
    {
        _GamepadableThingy = gamepadablethingy;
    }

    void 
    OnThingyDied()
    {
        _MoveableThingy.Died -= OnThingyDied;

        DisableInput();
        IsDead = true;
        _MoveableThingy = null; //Is it the right call site?
        Died?.Invoke();
    }

    internal void
    ControlMenu()
    {
        _Input.SwitchToMenuState(_GamepadableThingy);
    }

    internal void
    StartPlaying()
    {
        _Input.SwitchToGameState(_MoveableThingy);
    }

    internal void
    WaitToPlay()
    {
        _Input.SwitchToBeginGameState(_MoveableThingy);
    }

    internal void
    DisableInput()
    {
        _Input.SwitchToDisabledState();
    }
}
