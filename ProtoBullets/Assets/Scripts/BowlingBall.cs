using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlingBall : MonoBehaviour
{
    public Rigidbody2D rigidBody;
    public Transform BallTransform;

    public float rotationSpeed;

    void FixedUpdate()
    {
        var vel = rigidBody.velocity;
        if (vel.magnitude < 0.01f)
            return;

        // Very rough copied rotation of ball based on parents velocity
        Vector3 worldDirectionToPointForward = vel.normalized;
        Vector3 localDirectionToPointForward = Vector3.right;

        Vector3 currentWorldForwardDirection = transform.TransformDirection(
                localDirectionToPointForward);
        float angleDiff = Vector3.SignedAngle(currentWorldForwardDirection,
                worldDirectionToPointForward, Vector3.forward);

        transform.Rotate(Vector3.forward, angleDiff, Space.World);

        // This is completely wrong
        BallTransform.Rotate(BallTransform.up, rotationSpeed);
    }
}
