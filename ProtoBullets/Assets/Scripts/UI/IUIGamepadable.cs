public interface IUIGamepadable
{
    void EnableSelection();
    void DisableSelection();
    void GoLeft();
    void GoRight();
}
