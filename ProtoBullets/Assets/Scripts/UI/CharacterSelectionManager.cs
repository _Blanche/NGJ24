using UnityEngine;

public class CharacterSelectionManager : MonoBehaviour
{
    int _ValidatedPlayers;
    public CharacterSelection[] CharacterSelectors;

    public bool CheckAllPlayersReady()
    {
        bool Start = false;
        _ValidatedPlayers = 0;

        for (int i = 0; i < CharacterSelectors.Length; i++)
        {
            if (CharacterSelectors[i].selectionState == CharacterSelection.SelectionState.Selecting)
            {
                Debug.Log("A player is still selecting, give 'em some time.");
                return false;
            }

            if (CharacterSelectors[i].selectionState == CharacterSelection.SelectionState.Ready)
                _ValidatedPlayers++;
        }

        if (_ValidatedPlayers >= 2)
        {
            Start = true;
        }
        else
        {
            Debug.Log("You need at least 2 players to play.");
        }

        return Start;
    }

    void 
    Awake()
    {
        for (int i = 0; i < CharacterSelectors.Length; i++)
        {
            Director.Players[i].AttachUISelection(CharacterSelectors[i]);
        }
        Director.Menu_LoadingScene();
    }

    void 
    Update()
    {
        if (Input.GetButtonDown("Start0"))
        {
            if (CheckAllPlayersReady())
            {
                Director.Menu_SetCurrentUsersCount(_ValidatedPlayers);
                Director.Game_Start();
            }    
        }
    }
}
