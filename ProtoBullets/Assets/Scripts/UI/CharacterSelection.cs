using UnityEngine;

public class CharacterSelection : MonoBehaviour, IUIGamepadable
{
    public GameObject IdleObj;
    public GameObject SelectObj;
    public GameObject[] characters;
    bool _ValidatingPress;
    private int selectedCharacter = 0;
    public int playerNumber = 1;
    public SelectionState selectionState = SelectionState.Idle;

    public enum SelectionState
    {
        Idle = 0,
        Selecting = 1,
        Ready = 2
    }
    private void Start()
    {
        selectedCharacter = playerNumber - 1;
    }

    public void NextCharacter()
    {
        characters[selectedCharacter].SetActive(false);
        selectedCharacter = (selectedCharacter + 1) % characters.Length;
        characters[selectedCharacter].SetActive(true);
    }
    public void PreviousCharacter()
    {
        characters[selectedCharacter].SetActive(false);
        selectedCharacter--;
        if (selectedCharacter < 0)
            selectedCharacter += characters.Length;
        characters[selectedCharacter].SetActive(true);
    }

    public void SetIdle()
    {
        selectionState = SelectionState.Idle;
        SelectObj.SetActive(false);
        IdleObj.SetActive(true);
    }

    public void SetSelecting()
    {
        selectionState = SelectionState.Selecting;
        IdleObj.SetActive(false);
        SelectObj.SetActive(true);
    }

    public void ToggleReady()
    {
        if (selectionState == SelectionState.Ready)
            selectionState = SelectionState.Selecting;

        if (selectionState == SelectionState.Selecting)
            selectionState = SelectionState.Ready;

        GetComponentInParent<CharacterSelectionManager>().CheckAllPlayersReady();
    }

    public void Validating()
    {
        selectionState = SelectionState.Ready;
        GetComponentInParent<CharacterSelectionManager>().CheckAllPlayersReady();
    }

    public void Cancelling()
    {
        selectionState = SelectionState.Selecting;
    }

    public void 
    EnableSelection()
    {
        if (!_ValidatingPress)
        {
            _ValidatingPress = true;
            SetSelecting();
        }
        else
        {
            Validating();
        }
    }

    public void 
    DisableSelection()
    {
        if (selectionState == SelectionState.Ready)
        {
            Cancelling();
        }
        else if (selectionState == SelectionState.Selecting)
        {
            _ValidatingPress = false;
            SetIdle();
        }
    }

    public void 
    GoLeft()
    {
        PreviousCharacter();
    }

    public void 
    GoRight()
    {
        NextCharacter();
    }
}
