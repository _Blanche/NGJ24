using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalEvents
{
    public static Action<GEvent> onEvent;

    public static void TriggerEvent(GEvent gEvent)
    {
        onEvent.Invoke(gEvent);
    }
}

public struct GEvent
{
    public GEventType type;
    public GameObject sender;
    public string sValue;
    public int iValue;
    public float fValue;
    public bool bValue;
    public Vector3 vector;
}

public enum GEventType
{
    DIE,
    SPAWN,
    SHIELD_REFLECT,
    WALL_BOUNCE,
    HIT,
    SHIELD_SHOVE
}

