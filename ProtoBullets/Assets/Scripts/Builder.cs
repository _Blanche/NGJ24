using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Builder", menuName = "ScriptableObjects/Builder")]
public class Builder : ScriptableObject
{
    public GameObject PlayerPawnPrefab;
    public GameObject DPinPrefab;

    public IMoveable
    SpawnPlayerPawn(Transform[] spawnpoints, int playerindex)
    {
        Character PlayerPawn = GameObject.Instantiate(PlayerPawnPrefab, spawnpoints[playerindex].position, spawnpoints[playerindex].rotation).GetComponent<Character>();
        BowlingPinModel PinPrefab = GameObject.Instantiate(DPinPrefab, spawnpoints[playerindex].position, spawnpoints[playerindex].rotation).GetComponent<BowlingPinModel>();
        PinPrefab.Initialise(PlayerPawn);
        //PlayerPawn.Initialise();

        return PlayerPawn;
    }
}