using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heartFollower : MonoBehaviour
{
    public Vector3 offset;
    public Transform followerTarget;

    [SerializeField] private List<GameObject> _hearts;

    [SerializeField] private Character _targetCharacter;
    
    
    private void Start()
    {
        transform.SetParent(null);
    }

    private void Update()
    {
        transform.position = followerTarget.position + offset;

        for (int i = 0; i < _hearts.Count; i++)
        {
            _hearts[i].SetActive(_targetCharacter.GetHealth() > i);
        }
    }
}
