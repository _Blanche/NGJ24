using UnityEngine;

public class Initialiser : MonoBehaviour
{
    public Director Director;

    void 
    Awake()
    {
        Director.Initialise();
    }
}
