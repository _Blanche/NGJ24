using System;
using System.Collections;
using Prime31;
using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour, IMoveable
{
    public event Action Died;

    [SerializeField] private float _respawnTime;
    [SerializeField] private Transform _spawnpoint;
    [SerializeField] private float gamepadRotationOffset;

    
    Rigidbody2D _RB2D;

    [SerializeField] private CharacterController2D _characterController2D;
    
    public float Speed;

    public int initialHealth;
    [SerializeField] private UnityEvent _onPlayerDied;
    [SerializeField] private UnityEvent _onPlayerTookDamage;
    private int health;

    void 
    Awake()
    {
        health = initialHealth;
        _RB2D = GetComponent<Rigidbody2D>();
        _characterController2D = GetComponent<CharacterController2D>();
    }

    public void
    Move(Vector2 direction, float magnitude)
    {
//        Debug.Log(magnitude);
        if (magnitude < 0.05f || charging)
        {
            //Debug.Log("Stop Moving");
            _RB2D.velocity = Vector2.MoveTowards(_RB2D.velocity, Vector2.zero, Time.deltaTime * 5f);
            return;
        }
        var velocity = direction * magnitude * Speed * Time.deltaTime;
        // _RB2D.MovePosition(_RB2D.position + ());1

        // _RB2D.velocity = velocity;
        
        _characterController2D.move(velocity);
        
        // float targetRotation = Mathf.Atan2(-velocity.x, velocity.y) * Mathf.Rad2Deg;
        // _RB2D.rotation = targetRotation;
    }
    public void DontMove()
    {

    }

    public void
    Look(Vector2 target, Devices deviceused)
    {

        // return;
        if (deviceused == Devices.Mouse
         || deviceused == Devices.KeyboardNMouse)
        { target -= _RB2D.position; }
        
        float targetRotation = Mathf.Atan2(-target.y, -target.x) * Mathf.Rad2Deg;
        if (deviceused == Devices.Gamepad)
        {
            targetRotation += gamepadRotationOffset;
        }

        _RB2D.rotation = targetRotation;
    }

    public void OnAButton()
    {
        DoShieldPush();
    }

    public float invunerableEnd = 0f;
    public bool charging = false;

    void DoShieldPush()
    {
        if (!charging)
            StartCoroutine(ShieldPushRoutine());
    }

    public float shieldPushChargeDuration = 0.5f, shieldPushDuration = 0.4f;
    public Vector2 shieldPushSize = new Vector2(4, 4);
    public LayerMask shieldPushMaskMask;
    public GameObject shieldArea;

    IEnumerator ShieldPushRoutine()
    {
        charging = true;
        GlobalEvents.TriggerEvent(new GEvent() { type = GEventType.SHIELD_SHOVE, iValue = 0, sender = gameObject });
        
        yield return new WaitForSeconds(shieldPushChargeDuration);
        GlobalEvents.TriggerEvent(new GEvent() { type = GEventType.SHIELD_SHOVE, iValue = 1, sender = gameObject });
        shieldArea.SetActive(true);
        RaycastHit2D[] hits = Physics2D.BoxCastAll(transform.position + (transform.up * 2), shieldPushSize, transform.rotation.z, transform.up, 0.1f, shieldPushMaskMask);
        {
            Debug.Log("Hit " + hits.Length + " objects");
            foreach (RaycastHit2D hit in hits)
            {
                Bullet b = hit.collider.GetComponent<Bullet>();
                if (b != null)
                {
                    b.currentDirection = transform.up.normalized;
                    b.ShieldBashed();
                }
            }
        }
        yield return new WaitForSeconds(shieldPushDuration);
        GlobalEvents.TriggerEvent(new GEvent() { type = GEventType.SHIELD_SHOVE, iValue = 2, sender = gameObject });
        shieldArea.SetActive(false);
        charging = false;
    }

    public void DealDamage(int damage)
    {
        if (Time.time < invunerableEnd)
            return;

        invunerableEnd = Time.time + 2;
        health -= damage;
        _onPlayerTookDamage.Invoke();
        if (health <= 0)
        {
            KillPlayer();
        }
        else
        {
            var hitEvent = new GEvent();
            hitEvent.type = GEventType.HIT;
            hitEvent.sender = gameObject;
            GlobalEvents.TriggerEvent(hitEvent);
        }
    }

    
    private void KillPlayer()
    {
        KillParticles.instance.ShowKill(transform.position);
        GlobalEvents.TriggerEvent(new GEvent() { type = GEventType.DIE, sender = gameObject });
        transform.position = new Vector3(9999,9999,9999);
        StartCoroutine(RespawnAfterDelay());
        _onPlayerDied.Invoke();
    }

    public IEnumerator RespawnAfterDelay()
    {
        yield return new WaitForSeconds(_respawnTime);

        invunerableEnd = Time.time + 2;
        health = initialHealth;
        transform.position = _spawnpoint.position;
        transform.rotation = _spawnpoint.rotation;
    }

    internal int GetHealth()
    {
        return health;
    }
}
