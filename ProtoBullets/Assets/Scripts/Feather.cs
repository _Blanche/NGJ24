using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feather : MonoBehaviour
{
    public LineRenderer line;
    public GameObject rootPosition;
    public Transform pin;


    public List<Vector3> offsets = new List<Vector3>();

    private void OnEnable()
    {
        for (int i = 0; i < line.positionCount; i++)
        {
            line.SetPosition(i, rootPosition.transform.position + offsets[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < line.positionCount; i++)
        {
            float lerpAmount = 1f;
            if (i > 0)
            {
                lerpAmount = Mathf.Lerp(Time.deltaTime * 20, Time.deltaTime * 5, ((float)i)/line.positionCount);
            }
            line.SetPosition(i, Vector3.Lerp(line.GetPosition(i),
               rootPosition.transform.position +
               (pin.forward * offsets[i].y) +
               (pin.right * offsets[i].z), lerpAmount));
        }
    }
}
