using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [SerializeField] private Bullet _bulletPrefab;
    [SerializeField] private float _shootCooldown;

    [SerializeField] private float _initialShootCooldown;
    
    [SerializeField] private List<Transform> spawnPoints;

    private float shootTimer = 0;

    private void Start()
    {
        shootTimer = _initialShootCooldown;
    }

    void Update()
    {
        shootTimer -= Time.deltaTime;
        if (shootTimer <= 0)
        {
            Shoot();
            shootTimer = _shootCooldown;
        }
    }

    public void Shoot()
    {
        foreach (var spawnPoint in spawnPoints)
        {
            var bulletInstance = Instantiate(_bulletPrefab, spawnPoint.position, Quaternion.identity);
            var direction = (spawnPoint.position - transform.position).normalized; 
            bulletInstance.Initialize(direction);
        }
        
    }
}
