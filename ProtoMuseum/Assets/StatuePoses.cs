using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Unity.VisualScripting;
using static Unity.Burst.Intrinsics.X86;

public class StatuePoses : MonoBehaviour
{
    public List<Sprite> legPoses, bodyPoses;

    public SpriteRenderer bodyAction1, bodyAction2, bodyAction3, legAction1, legAction2, legAction3;

    public SpriteRenderer statueBody, statueLegs;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SpritePicked(bodyAction1.sprite, legAction1.sprite);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SpritePicked(bodyAction2.sprite, legAction2.sprite);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SpritePicked(bodyAction3.sprite, legAction3.sprite);
        }
    }

    public void SpritePicked(Sprite body, Sprite legs)
    {
        if (statueLegs.sprite == legPoses[0])
        {
            if (legs == legPoses[1])
                transform.position += Vector3.left;
            else if (legs == legPoses[2])
                transform.position += Vector3.right;
        }

        if (statueLegs.sprite == legPoses[1])
        {
            if (legs == legPoses[2])
                transform.position += Vector3.left;
            else if (legs == legPoses[0])
                transform.position += Vector3.right;
        }

        if (statueLegs.sprite == legPoses[2])
        {
            if (legs == legPoses[0])
                transform.position += Vector3.left;
            else if (legs == legPoses[1])
                transform.position += Vector3.right;
        }

        statueLegs.sprite = legs;
        statueBody.sprite = body;

        transform.localScale = Vector3.one * 0.75f;
        LeanTween.scale(gameObject, Vector3.one, 0.2f).setEaseOutBounce();

        RandomizeActions();
    }

    public void RandomizeActions()
    {
        List<Sprite> bodySprites = RandomizeSprites(bodyPoses);

        bodyAction1.sprite = bodySprites[0];
        bodyAction2.sprite = bodySprites[1];
        bodyAction3.sprite = bodySprites[2];

        List<Sprite> legSprites = RandomizeSprites(legPoses);

        legAction1.sprite = legSprites[0];
        legAction2.sprite = legSprites[1];
        legAction3.sprite = legSprites[2];
    }

    public Pose GetARandomPose()
    {
        List<Sprite> bodySprites = RandomizeSprites(bodyPoses);
        List<Sprite> legSprites = RandomizeSprites(legPoses);
        return new Pose(bodySprites[0], legSprites[0]);
    }

    public List<Sprite> RandomizeSprites(List<Sprite> poses)
    {
        List<Sprite> sprites = new List<Sprite>() { };
        foreach (Sprite s in poses)
            sprites.Add(s);

        for (int i = 0; i < 50; i++)
        {
            int v1 = Random.Range(0, sprites.Count), v2 = Random.Range(0, sprites.Count);
            Sprite s1 = sprites[v1];
            sprites[v1] = sprites[v2];
            sprites[v2] = s1;
        }

        return sprites;
    }
}

public class Pose
{
    public Sprite body, legs;

    public Pose(Sprite body, Sprite legs)
    {
        this.body = body;
        this.legs = legs;
    }
}