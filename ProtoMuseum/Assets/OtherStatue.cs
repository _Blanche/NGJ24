using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherStatue : MonoBehaviour
{
    public StatuePoses player;

    public SpriteRenderer statueBody, statueLegs;

    public Pose myPose;

    public bool broken = false;
    public float triggerDistance = 2f;

    private void OnEnable()
    {
        myPose = player.GetARandomPose();
        statueBody.sprite = myPose.body;
        statueLegs.sprite = myPose.legs;
    }

    private void Update()
    {
        if (!broken && Mathf.Abs(player.transform.position.x - transform.position.x) < triggerDistance)
        {
            broken = true;
            StartCoroutine(BreakRoutine());
        }
    }

    IEnumerator BreakRoutine()
    {
        LeanTween.rotate(gameObject, new Vector3(0, 0, 90), 2f).setEase(LeanTweenType.easeInQuad);
        yield return new WaitForSeconds(2);
        statueBody.enabled = false;
        statueLegs.enabled = false;

        Debug.Log("Guard approaching");
        yield return new WaitForSeconds(10);
        Debug.Log("Guard is here");
        if (myPose.body == player.statueBody.sprite && myPose.legs == player.statueLegs.sprite)
        {
            Debug.Log("You're OK");
        }
        else
        {
            Debug.Log("THAT'S NOT THE RIGHT STATUE");
        }
    }
}
